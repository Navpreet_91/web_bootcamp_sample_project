const mongoose = require("mongoose");

mongoose
  .connect("mongodb://localhost:27017/moviesDB", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to db Successfully");
  })
  .catch((err) => {
    console.log("Connecting ERROR!!!!");
    console.log(err);
  });

const moviesSchema = new mongoose.Schema({
  title: String,
  year: Number,
  score: Number,
  rating: String,
});

const Movie = mongoose.model("Movie", moviesSchema);

/* // Movie.insertMany([
//   { title: "Dhoom", year: 2000, score: 7, rating: "R" },
//   { title: "Mardani", year: 2005, score: 8, rating: "R" },
//   { title: "Dhoom 2", year: 1998, score: 7, rating: "PG" },
//   { title: "Welcome", year: 1995, score: 8.5, rating: "R" },
//   { title: "PK", year: 2019, score: 9, rating: "PG-13" },
//   { title: "3 Idiots", year: 2007, score: 9, rating: "R" },
// ]).then(data=>{
//   console.log('Data saved succesfully!!');
//   console.log(data);
// })  .catch((err) => {
//   console.log("Saved ERROR!!!!");
//   console.log(err);
// }); */