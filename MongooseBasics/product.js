const mongoose = require("mongoose");

mongoose
  .connect("mongodb://localhost:27017/shopApp", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connected to db Successfully");
  })
  .catch((err) => {
    console.log("Connecting ERROR!!!!");
    console.log(err);
  });

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    maxlength: 10,
  },
  price: {
    type: Number,
    min:70
  },
  onSale: {
    type: Boolean,
    default: false,
  },
  categories: [String],
  qty: {
    online: {
      default: 0,
      type: Number,
    },
    inStoreL: {
      default: 0,
      type: Number,
    },
  },
  size:{
      type:String,
      enum:['S','M','L']
  }
});

productSchema.methods.greet=function(){
    console.log('Helloo!!!!!!')
}

const Product = mongoose.model("Product", productSchema);

const findProduct=async()=>{
   const findPro= await Product.findOne({name:'Bike helmet'})
   findPro.greet();
}

findProduct();
// const bike = new Product({ name: 'Tire Pump', price: 150,categories:['Cycling']})
// bike.save()
//     .then(data => {
//         console.log('It worked')
//         console.log(data)
//     }).catch(e=>{
//     console.log('Error!!!');
//     console.log(e)
// })

// Product.findOneAndUpdate(
//   { name: "Tire Pump" },
//   { price: 101 },
//   { new: true, runValidators: true }
// )
//   .then((data) => {
//     console.log("Updated");
//     console.log(data);
//   })
//   .catch((e) => {
//     console.log("Error!!!");
//     console.log(e);
//   });
