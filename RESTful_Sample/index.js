const express = require("express");
const app = express();
const path = require("path");
const { v4: uuid } = require("uuid");
const methodOverride = require("method-override");

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(methodOverride("_method"));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

let comments = [
  {
    id: uuid(),
    username: "Nav",
    comment: "How r you?",
  },
  {
    id: uuid(),
    username: "Navi",
    comment: "Awesome!!!!",
  },
  {
    id: uuid(),
    username: "Navu",
    comment: "Sounds great!!!",
  },
  {
    id: uuid(),
    username: "Navpreet",
    comment: "I like this post",
  },
];

//display all comments
app.get("/comments", (req, res) => {
  res.render("comments/index", { comments });
});

//create a new comment add it to array to get it display
app.get("/comments/new", (req, res) => {
  res.render("comments/new");
});

app.post("/comments", (req, res) => {
  const { username, comment } = req.body;
  comments.push({ username, comment, id: uuid() });
  res.redirect("/comments");
});

//details of a specific comment
app.get("/comments/:id", (req, res) => {
  const { id } = req.params;
  const comment = comments.find((c) => c.id === id);
  res.render("comments/show", { comment });
});

//edit route for a comment
app.patch("/comments/:id", (req, res) => {
  const { id } = req.params;
  const newCommentText = req.body.comment;
  const foundComment = comments.find((c) => c.id === id);
  foundComment.comment = newCommentText;
  res.redirect("/comments");
});

//form to edit the comment
app.get("/comments/:id/edit", (req, res) => {
  const { id } = req.params;
  const comment = comments.find((c) => c.id === id);
  res.render("comments/edit", { comment });
});

//delete a comment
app.delete("/comments/:id", (req, res) => {
  const { id } = req.params;
  // const foundComment = comments.find((c) => c.id === id);
  comments = comments.filter((c) => c.id !== id);
  res.redirect('/comments')
});

app.listen(3000, () => {
  console.log("Listening to port 3000!!!!!");
});
