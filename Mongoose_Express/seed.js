const mongoose = require("mongoose");
const Product = require("./models/product");

mongoose
    .connect("mongodb://localhost:27017/farmStand", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log("Connected to mongo Successfully");
    })
    .catch((err) => {
        console.log("Mongo Connecting ERROR!!!!");
        console.log(err);
    });

//   const p=new Product({
//       name:'Grapes',
//       price:16,
//       category:'fruit'
//   })

//   p.save().then(p=>{
//       console.log(p)
//   }).catch(err=>{
//       console.log(err)
//   })

const seedproducts = [
    {
        name: "Eggplant",
        price: 1.99,
        category: "vegetable",
    },
    {
        name: "Melon",
        price: 3.99,
        category: "fruit",
    },
    {
        name: "Watermelon",
        price: 3.99,
        category: "fruit",
    },
    {
        name: "Milk",
        price: 4.99,
        category: "dairy",
    },
];

Product.insertMany(seedproducts)
    .then((r) => console.log(r))
    .catch((e) => {
        console.log(e);
    });
