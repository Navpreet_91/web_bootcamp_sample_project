const express = require("express");

const app = express();

// app.use((req,res)=>{
//     console.log('Welcome Navpreet!!!');
//     res.send("Hey Navpreet!!!!")
// })
app.get("/r/:subreddit", (req, res) => {
  res.send("Its subreddit");
});
app.get("/search", (req, res) => {
  const {q} = req.query;
  res.send(`<h1>Search results for :${q}</h1>`);
});

app.get("/cats", (req, res) => {
  console.log("Cats page");
  res.send("Meow!!!!!!");
});

app.get("/", (req, res) => {
  res.send("Welcome to home page,Hi!!!!!!!!!!");
});

app.get("*", (req, res) => {
  res.send("Something went wrong with the URL!!!");
});
app.listen(3000, () => {
  console.log("Listening on port 3000");
});
